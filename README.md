# Vehicle Loan

Aplikasi Vehicle Loan merupakan aplikasi simulasi kredit peminjaman kendaraan. 

## Requirement
- java 1.8 or above

## Tech Stack
- Programming Language: **Java**
- DevOps Tool: **Gitlab**
- Container: **Docker**


## Installation
1. Clone project: https://gitlab.com/felix.rustan/credit_simulator.git
2. Dari **root** folder ketikan command berikut
* **Linux** 
  * Jika menggunakan file input   
  ```bash
    bin/credit_simulator file_inputs.txt
  ```
   * Jika menggunakan terminal
  ```bash
    bin/credit_simulator
  ```
* **Windows**  
   * Jika menggunakan file input   
  ```bash
    java -jar bin/credit_simulator.jar file_inputs.txt
  ```
   * Jika menggunakan terminal
  ```bash
  java -jar bin/credit_simulator.jar
  ```
    
## Testing
Untuk Unit Testing dapat dilakukan dengan membuka **src/test/java/bluetest/installment.java** kemudian menjalakan-nya bisa menggunakan IDE yang mendukung Java. Disini saya menggunakan Eclipse, maka cukup dengan Run as > JUnit Test pada Root folder



## Petunjuk Penggunaan

Pada aplikasi ini user diberikan 3 menu yaitu sebagai berikut:

1. [Simulasi Kredit](#simulasi-kredit)
2. [Load Data](#load-data)
3. [Melihat Status Pengajuan](#melihat-status-pengajuan)
4. [Keluar](#keluar)

```bash
> [input berupa angka 1- 4]
```

## Simulasi Kredit
- Pengguna diminta untuk mengisi jenis kendaraan **[Mobil/Motor]**
```bash
> [input Mobil/Motor]
```
- Pengguna diiminta untuk mengisi  tipe kendaraan **[Baru/Bekas]**
```bash
> [input Baru/Bekas]
```
- Pengguna diiminta untuk mengisi  tahun kendaraan **[4 digit angka]**
    * Jika **tipe kendaraan** adalah baru makan tahun kendaraan tidak boleh < **tahun   sekarang - 1**
```bash
> [input berupa 4 digit angka]
```
- Pengguna dminta untuk mengisi jumlah pinjaman total **[Angka]** 
```bash
> [input berupa angka]
```
- Pengguna dminta untuk mengisi tenor pinjaman **[1-6 Tahun]**
```bash
> [input berupa angka]
```
- Pengguna dminta untuk mengisi jumlah DP **[angka harus <= jumlah pinjaman]**
```bash
> [input berupa angka]
```
- Program akan menampilkan simulasi kredit-nya

## Load Data
Pada menu ini dasarnya sistem akan mengsimulasikan kredit langsung dari *endpoint* http://www.mocky.io/v2/5d11a58d310000b23508cd62

## Melihat Status Pengajuan
Pada menu ini pengguna akan ditampilkan riwayat pengajuan kredit

## Keluar
Pengguna akan keluar dari aplikasi