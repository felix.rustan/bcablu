FROM openjdk
COPY bin/*.jar /
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/credit_simulator.jar"]
